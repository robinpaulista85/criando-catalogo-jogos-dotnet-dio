Author: [Robinson Dias](https://github.com/robinson-1985)

# criando-catalogo-de-jogos-dotnet-dio

- Nesse projeto construímos uma arquitetura base para uma aplicação .NET do zero.

* Curso: Criando um catálogo de jogos usando boas práticas de arquitetura com .NET
* link: https://web.digitalinnovation.one/lab/arquitetura-essencial-de-api-com-net/learning/e4600dc8-0e68-45cd-82e6-c048c0946f7c

- Especialista expert da DIO: Thiago Campos de Oliveira.
